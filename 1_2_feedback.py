from itertools import combinations as nCr


def feedback_is_1(guess : str, letter5: list[str]) -> list[str]:
    letter5_ = []
    for i in letter5: 
        if len(set(guess)&(set(i))) != 0:
            letter5_.append(i)
    return letter5_


def feedback_is_2(guess : str , letter5: list[str]) -> list[str]:
    possible_letters = [set(_) for _ in nCr(guess , 2)]
    letter5_ = []
    for i in letter5:
        for letters in possible_letters:
            if len(letters & set(i)) >= 2 :
                letter5_.append(i)
    return list(set(letter5_))




