from functools import reduce
from itertools import permutations as nPr

def feedback_0(guessed_word: str, word_list: str) -> list[str]:
    characters = reduce(lambda acc, char: acc + [char], guessed_word, [])
    filtered_words = [word for word in word_list if all(char not in characters for char in word)]
    return filtered_words

#guessed_word = "hello"
#word_list = ["apple", "banana", "orange", "grape", "kiwi", "hello", "house"]
#filtered_words = feedback_0(guessed_word, word_list)
#print(filtered_words)   


        
       



