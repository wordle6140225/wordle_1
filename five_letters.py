from itertools import permutations as nPr
def possible_ans(word: str) -> list[str]:
	five_letters = nPr(word,5)
	ans_list = [''.join(i) for i in five_letters]
	return list(set(ans_list))

