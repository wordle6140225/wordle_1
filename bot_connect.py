import requests as rq
import json


mm_url = "https://we6.talentsprint.com/wordle/game/"

register_url = mm_url + "register"

register_dict = {'mode' : "Mastermind" , "name" : "ducklings" }
# register_with = json.dumps(register_dict)
session = rq.Session()
r = session.post(register_url , json=register_dict)
me = r.json()['id']

creat_url = mm_url + "create"
creat_dict = {"id" : me , "overwrite": True}

rc = session.post(creat_url , json=creat_dict)
guess_url = mm_url + 'guess'

# words =  [word.strip() for word in open("5letters.txt")] ashokans code
words =[]
for word in open("5letters.txt") :
    if len(set(''.join(word.strip()))) == 5 :
        words.append(word.strip())
guess = words[0]

def guessing() :
    feedback_msg = session.post(guess_url , json = guess)
    # feedback_msg looks like - {'feedback' : 1 , 'message' : '2 guesses were made so far'}
    feedback  = feedback_msg['feedback']
    if feedback == 5 :
        words = possible_ans(guess)
    else :
        words = reduce(words,guess,feedback)
    guess = words[0]


def reduce ( words : list[str], guess :str ,feedback : int ) -> list[str] :
    #copies = [set(word) for word in words]
    guess = set(guess)
    new = []
    for word in words :
        print(word)
        if len(set(word) & guess) == feedback :
            new.append(word)
    return new

def possible_ans(word: str) -> list[str]:
	five_letters = nPr(word,5)
	ans_list = [''.join(i) for i in five_letters]
	return list(ans_list)


from itertools import permutations as nPr
def possible_ans(word: str) -> list[str]:
	five_letters = nPr(word,5)
	ans_list = [''.join(i) for i in five_letters]
	return list(set(ans_list))

'''
from itertools import combinations as nCr

from functools import reduce


def feedback_0(guessed_word: str, word_list: str) -> list[str]:
    characters = reduce(lambda acc, char: acc + [char], guessed_word, [])
    filtered_words = [word for word in word_list if all(char not in characters for char in word)]
    return filtered_words



def feedback_is_1(guess : str, letter5: list[str]) -> list[str]:
    letter5_ = []
    for i in letter5: 
        if len(set(guess)&(set(i))) != 0:
            letter5_.append(i)
    return letter5_
'''
'''
def feedback_is_2(guess : str , letter5: list[str]) -> list[str]:
    possible_letters = [set(_) for _ in nCr(guess , 2)]
    letter5_ = []
    for i in letter5:
        for letters in possible_letters:
            if len(letters & set(i)) == 2 :
                letter5_.append(i)
    return list(set(letter5_))
'''


#words = ["sneha","hello","wxyzk","stklm","heslo"]
#guess = "sneha"


'''
