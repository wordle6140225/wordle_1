
def load_data(data_file : str) -> list[str]:
    with open(data_file) as f:
        words = [line.strip() for line in f ]
    return words
